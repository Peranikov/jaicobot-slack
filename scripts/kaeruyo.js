module.exports = function(controller, bot) {
  controller.hears(['.*かえる.*', '.*帰る.*'], 'ambient', (bot, message) => {
    bot.reply(message, randomMessage());
  });

  function randomMessage() {
    const messages = [
      "おつかれもじ〜",
      "気をつけるもじよ〜",
      "わかったもじ〜",
      "はいもじ〜"
    ];

    return messages[Math.floor(Math.random() * messages.length)];
  }
}
