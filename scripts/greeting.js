module.exports = function(controller, bot) {
  controller.hears(['.*おはよ.*'], 'direct_message,direct_mention,mention', (bot, message) => {
    bot.reply(message, 'おはようもじ〜');
  });

  controller.hears(
    ['.*おやすみ.*', '.*お休み.*'],
    'direct_message,direct_mention,mention',
    (bot, message) => {
      bot.reply(message, 'おやすみもじ〜');
    }
  );
}