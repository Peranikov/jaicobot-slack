module.exports = function(controller, bot, channels) {
  const CronJob = require("cron").CronJob;
  const tz = "Asia/Tokyo";
  const generalCh = channels["general"];

  new CronJob("00 00 15 * * *", function() {
    bot.say({text: "おやつの時間もじよ〜", channel: generalCh})
  }, null, true, tz);
}