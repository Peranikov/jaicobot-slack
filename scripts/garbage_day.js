module.exports = function(controller, bot, channels) {
  const CronJob = require("cron").CronJob;
  const moment = require("moment");
  const tz = "Asia/Tokyo";
  const generalCh = channels["general"];

  new CronJob("00 00 22 * * 1", function() {
    bot.say({text: "明日は燃えるゴミの日もじよ〜", channel: generalCh})
  }, null, true, tz);

  new CronJob("00 00 22 * * 2", function() {
    const tomorrowDays = moment().add(1, 'days').date();
    if (tomorrowDays < 8 || (14 < tomorrowDays && tomorrowDays < 22)) {
      bot.say({text: "明日は缶/ビン/紙の日もじよ〜", channel: generalCh})
    } else {
      bot.say({text: "明日は缶/ビンの日もじよ〜", channel: generalCh})
    }
  }, null, true, tz);

  new CronJob("00 00 22 * * 4", function() {
    bot.say({text: "明日はプラスチックゴミの日もじよ〜", channel: generalCh})
  }, null, true, tz);

  new CronJob("00 00 22 * * 5", function() {
    bot.say({text: "明日は燃えるゴミの日もじよ〜", channel: generalCh})
  }, null, true, tz);

  new CronJob("00 00 00 05 06 *", function() {
    bot.say({text: "@minato 誕生日おめでとうもじ!", channel: generalCh})
  }, null, true, tz);

  new CronJob("00 00 00 14 01 *", function() {
    bot.say({text: "@yuto 誕生日おめでとうもじ!", channel: generalCh})
  }, null, true, tz);
}