module.exports = function(controller, bot, channels) {
  const exec = require('child_process').exec;
  const channel = channels["bot-log"];

  controller.hears(['update'], 'direct_message,direct_mention', (bot, message) => {
    try {
      bot.say({text: "更新するもじ", channel: channel})
      exec('git pull', (error, stdout, stderr) => {
        console.log("git pull")
        console.log(`stdout: ${stdout}`)
        console.log(`stderr: ${stderr}`)

        if (error !== null) {
          bot.say({text: `だめだったもじ: ${error}`, channel: channel})
          return
        }

        output = stdout + ''
        if (/Already up\-to\-date/.test(output)) {
          bot.say({text: "最新だったもじ", channel: channel})
          return
        }

        bot.say({text: "npm installするもじ", channel: channel})
        exec('npm install', (error, stdout, stderr) => {
          console.log("npm install")
          console.log(`stdout: ${stdout}`)
          console.log(`stderr: ${stderr}`)

          bot.say({text: "再起動するもじ", channel: channel})
          process.exit();
        });
      });
    } catch(e) {
      console.error(e)
      bot.say({text: `だめだったもじ: ${e}`, channel: channel})
    }
  });
}
