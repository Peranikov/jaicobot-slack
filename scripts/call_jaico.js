module.exports = function(controller, bot) {
  controller.hears(['.*じゃいこ.*'], 'direct_message,direct_mention,mention', (bot, message) => {
    bot.reply(message, randomMessage());
  });

  function randomMessage() {
    const messages = [
      "じゃいこもじよ〜",
      "なにもじか〜",
      "ちょっと忙しいもじ",
      "はいもじ",
      "きたよ〜",
      "ひぇ〜"
    ];

    return messages[Math.floor(Math.random() * messages.length)];
  }
}
