"use strict"

if (!process.env.token) {
    console.log('Error: Specify token in environment');
    process.exit(1);
}

const fs = require('fs');
const Botkit = require('botkit');
const channels = {
  "bot-test": "G0LETEK0E",
  "bot-log": "C0P0FEZ6C",
  "general": "C0L59GQ2U"
}

const controller = Botkit.slackbot({
  debug: false,
});

const bot = controller.spawn({
  token: process.env.token
}).startRTM((err,bot,payload) => {
  if (err) {
    throw new Error('Could not connect to Slack');
  }
});

// autoload scripts
const scriptsDir = './scripts/'

fs.readdir(scriptsDir, (err, files) => {
  if (err) throw err;

  files.filter((file) => {
    return fs.statSync(scriptsDir + file).isFile() && /.*\.js$/.test(file);
  }).forEach((file) => {
    file = scriptsDir + file
    console.log(`require ${file}`)
    require(file)(controller, bot, channels)
  });
});
